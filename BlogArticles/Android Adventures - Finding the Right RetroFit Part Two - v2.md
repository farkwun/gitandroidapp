Well, well, well. We meet again.

Welcome to Part Two of Finding the Right RetroFit. Last time, we threw together a rudimentary Android application – used RetroFit to pull data from GitHub’s public repositories and display some of that data in a Text View.

Exciting. 

Today, we’re going to look at upgrading our application interface (if you can call it that… perhaps ‘face’ is more appropriate) from Basic to positively Rudimentary. 

That’s right, we’re going to display our data in a List View! We're going to want to use Picasso to handle our images. Just like in Part One, we just need to add a line into our Gradle script in order to use the library.

	compile 'com.squareup.picasso:picasso:2.3.3'

If you’ve been following along, you should have four classes in your application – the GitApi class, the GitModel class, the Owner class, and the MainActivity class. You should also have an XML layout file (activity_main) in your resources folder. 

To display our list we first need to add a ListView element to our activity_main.xml file, like so:

    <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:paddingBottom="@dimen/activity_vertical_margin"
    android:paddingLeft="@dimen/activity_horizontal_margin"
    android:paddingRight="@dimen/activity_horizontal_margin"
    android:paddingTop="@dimen/activity_vertical_margin"
    tools:context=".MainActivity"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:padding="6dip" >
    <TextView
        android:id="@+id/texv"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:maxLines = "100"
        android:scrollbars = "vertical"/>
    <ListView xmlns:android="http://schemas.android.com/apk/res/android"
        android:id="@+id/listView"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content" />
    </RelativeLayout>

You may have noticed we've kept our TextView from before - that's okay! We'll use it  to display error messages for us in case things get kookier than usual. 

We're also going to need to add a layout file for our list element. Create a new XML file in your res/layour folder for it. I called mine activity_list_view.xml

    <?xml version="1.0" encoding="utf-8"?>
    <RelativeLayout
        xmlns:android="http://schemas.android.com/apk/res/android"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="10dp"
        android:background="@android:color/white">
        <ImageView
            android:id="@+id/image_in_item"
            android:layout_width="100dp"
            android:layout_height="100dp"/>
        <TextView
            android:id="@+id/textview_in_item"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:layout_toRightOf="@+id/image_in_item"
            android:layout_marginLeft="10dp"/>
        <TextView
            android:id="@+id/textview_in_item_two"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="12sp"
            android:layout_toRightOf="@+id/image_in_item"
            android:layout_below="@+id/textview_in_item"
            android:layout_marginLeft="10dp"/>
    </RelativeLayout>

Each List Item will have an ImageView and two TextViews, but you can always add or remove elements at your leisure. 

For each repository we get from our JSON request, we're going to display the name, ID, and avatar_url (the URL pointing to the avatar of the repository's owner). If you're wondering what we're putting into our ImageView, I'm glad you asked (just remember who's calling the shots around here) - we'll be using another excellent open-source library, _Picasso_, to download and cache these avatar images and display them in our ListView. 

So, we've got a ListView and a layout for the items with which we're going to fill it. What now? 

We're going to use an Adapter to *inflate* (or render) a view for each of the repositories from our request. I called mine ListViewAdapter.

    public class ListViewAdapter extends BaseAdapter {

    LayoutInflater inflater;
    List<GitModel> GitModelList;


    public ListViewAdapter(LayoutInflater inflater, List<GitModel> GitModelList){
        this.inflater = inflater;

        this.GitModelList = GitModelList;
    }

    @Override
    public int getCount() {
        return GitModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return GitModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_list_view, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(inflater.getContext())
                .load(""+GitModelList.get(position).getOwner().getAvatar_url()) //""+gitmodelList.get(position).getOwner().getAvatar_url()
                .into(holder.image);

        holder.text.setText(" Name: "+GitModelList.get(position).getName()
                +"\t id: "+GitModelList.get(position).getId()+"\n");

        holder.texttwo.setText(""+GitModelList.get(position).getOwner().getAvatar_url());

        return convertView;

    }
    static class ViewHolder{
        @Bind(R.id.image_in_item)
        ImageView image;
        @Bind(R.id.textview_in_item)
        TextView text;
        @Bind(R.id.textview_in_item_two)
        TextView texttwo;

        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }

    public void setGitmodel(List<GitModel> gitmodelList){
        clearGitmodel();
        this.GitModelList = gitmodelList;
        notifyDataSetChanged();
    }

    public void setInflater(LayoutInflater inflater){
        this.inflater = inflater;
    }

    public void clearGitmodel(){
        this.GitModelList.clear();
    }
}

Don't worry if this doesn't make sense right now, we'll go over it piece by piece. 

Our ListViewAdapter constructor takes two arguments - an inflater and an ArrayList of GitModels. In order to implement our custom adapter, we also need to override four methods, getCount(), getItem(), getItemId, and finally getView. 

Briefly, the getCount method returns the number of items, which is set to the size() of your ArrayList. The getItem() and getItemId() methods returns an item given an ID and the ID of the current item, respectively. 

    @Override
    public int getCount() {
        return GitModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return GitModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


Before we get into the juicy stuff in the getView() method, let’s talk briefly about the ViewHolder class. We use a little bit of ButterKnife magic to instantiate a view in its constructor. We’re going to use this class in the getView() method. 

    static class ViewHolder{
        @Bind(R.id.image_in_item)
        ImageView image;
        @Bind(R.id.textview_in_item)
        TextView text;
        @Bind(R.id.textview_in_item_two)
        TextView texttwo;

        public ViewHolder(View view){
            ButterKnife.bind(this, view);
        }
    }

Our getView() method creates ViewHolder objects – but here is where we implement some better practices! In my first implementation, I instantiated a new ViewHolder every time the getView() method was called. Because we’re downloading images and caching them, we don’t want to have to do that every time we scroll up or down, which is what that would have done! 

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.activity_list_view, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

Now, we check if our View already exists before making a new one. In our getView() class, we also set the information that is to be displayed in each of our the Views within our List elements (an ImageView and two TextViews). You probably recognize our TextViews, but for our ImageView is where we use Picasso! Picasso downloads, caches, and places our image in the appropriate List element, and its where we use our avatar_url! 

        Picasso.with(inflater.getContext())
                .load(""+GitModelList.get(position).getOwner().getAvatar_url()) //""+gitmodelList.get(position).getOwner().getAvatar_url()
                .into(holder.image);

        holder.text.setText(" Name: "+GitModelList.get(position).getName()
                +"\t id: "+GitModelList.get(position).getId()+"\n");

        holder.texttwo.setText(""+GitModelList.get(position).getOwner().getAvatar_url());

Finally, we put in some setters so we can change the attributes of our Adapter, which is especially important for our GitModelList, as well as a clear() method that we use to clear out our ArrayList before setting it to a new one. 

    public void setGitmodel(List<GitModel> gitmodelList){
        clearGitmodel();
        this.GitModelList = gitmodelList;
        notifyDataSetChanged();
    }

    public void setInflater(LayoutInflater inflater){
        this.inflater = inflater;
    }

    public void clearGitmodel(){
        this.GitModelList.clear();
    }

The setInflater() and clearGitmodel() methods are relatively straightforward, but the setGitmodel is the important one. We clear our ArrayList<> before setting the new List, then we use the notifyDataSetChanged() method to 'tell' our ListView it's time to update itself. 

For our MainActivity, if you've got our code from Part One, we only need to make a few small tweaks. 

We're adding five lines before our onCreate() method:

    LayoutInflater inflater;
    List<GitModel> placeholderModel;
    ListViewAdapter listAdapter;
    @Bind(R.id.listView)
    ListView list;
    
An inflater, placeholder List, our ListViewAdapter, and a ButterKnife @Bind for our ListView element. 

After we set our Content View in the onCreate() method, we're going to initialize our inflater, placeholder, and finally our listAdapter like so:

        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        placeholderModel = new ArrayList<>();
        listAdapter = new ListViewAdapter(inflater, placeholderModel);

        list.setAdapter(listAdapter);
        
In my first implementation, I created a new Adapter each time we called the success() method, essentially recreating our entire ListView on each successful HTTP request. By intializing our Adapter here, we only need a single line in our success() method!

        listAdapter.setGitmodel(gitmodel);
        
Putting it all together, this is our MainActivity class:

    public class MainActivity extends Activity {

    String API = "https://api.github.com";                         //BASE URL
    LayoutInflater inflater;
    List<GitModel> placeholderModel;
    ListViewAdapter listAdapter;
    @Bind(R.id.listView)
    ListView list;
    @Bind(R.id.texv)
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);

        placeholderModel = new ArrayList<>();
        listAdapter = new ListViewAdapter(inflater, placeholderModel);

        list.setAdapter(listAdapter);
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(API).build();  //create an adapter for retrofit with base url

        GitApi git = restAdapter.create(GitApi.class);  //creating a service for adapter with our GET class

        git.getFeed(new Callback<List<GitModel>>() {
            @Override
            public void failure(RetrofitError error) {
                tv.setText(error.getMessage());
            }
            @Override
            public void success(final List<GitModel> gitmodel, Response response) {
                listAdapter.setGitmodel(gitmodel);
            }
        });
    }
}

And there we have it!  We've replaced our ugly TextView with a slightly-less-ugly ListView! We also got some familiarity with Picasso - if you'd like, you can try to load images into the ImageView using more traditional methods and see how Picasso streamlines image handling. 

Next week, we'll finish off this project by adding some *interactivity* to our List! So hold onto your seats and hopefully I'll be seeing you in Part Three: Enter the Dragon. 