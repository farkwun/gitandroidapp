package course.examples.retrofittest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Bryan Roiled on 2015-09-22.
 */
public class GitRepoWebActivity extends Activity {
    @Bind(R.id.webview)
    WebView mWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repo_webview);
        ButterKnife.bind(this);

        String url = getIntent().getStringExtra(getString(R.string.extra_url));
        mWebView.loadUrl(url);
        mWebView.setWebViewClient(new WebViewClient() {
            private ProgressDialog mProgressDialog;

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                mProgressDialog = new ProgressDialog(view.getContext());
                mProgressDialog.setMessage(getString(R.string.please_wait));
                mProgressDialog.setCancelable(true);
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                // Need this to avoid crashes when backing out of web activity too fast
                if (!isFinishing()) {
                    mProgressDialog.show();
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.e("TAG", "onPageEnded");

                mProgressDialog.dismiss();
            }
        });
    }
}