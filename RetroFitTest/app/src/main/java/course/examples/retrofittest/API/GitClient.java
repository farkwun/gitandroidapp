package course.examples.retrofittest.API;
/**
 * Created by Bryan Roiled on 2015-09-06.
 */

import java.util.List;

import course.examples.retrofittest.model.GitModel;
import course.examples.retrofittest.model.User;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

public class GitClient {
    private static GitApi sGitApiService;
    private final static String BASE_URL = "https://api.github.com";

    public static GitApi getGitClient() {
        if (sGitApiService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(BASE_URL)
                    .build();

            sGitApiService = restAdapter.create(GitApi.class);
        }

        return sGitApiService;
    }

    public interface GitApi {

        @GET("/repositories")
            //here is the other url part.best way is to start using /
        void getFeed(Callback<List<GitModel>> response);

        @GET("/users/{user}")
        void getUser(@Path("user") String user, Callback<User> response);

    }
}
