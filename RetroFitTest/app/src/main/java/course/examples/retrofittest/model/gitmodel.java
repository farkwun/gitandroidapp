package course.examples.retrofittest.model;

import com.google.gson.annotations.Expose;

public class GitModel {
    @Expose
    private String login;
    @Expose
    private Integer id;
    @Expose
    private String name;
    @Expose
    private Owner owner;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}
