package course.examples.retrofittest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import course.examples.retrofittest.API.GitClient;
import course.examples.retrofittest.model.User;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Bryan Roiled on 2015-09-27.
 */
public class UserProfileActivity extends Activity implements Validator.ValidationListener {
    private Validator mValidator;
    private ProgressDialog mProgressDialog;
    private boolean mIsContentLoaded;

    @Bind(R.id.profile_image)
    ImageView image;

    @NotEmpty
    @Bind(R.id.username_edit)
    EditText mUsernameEditText;

    @NotEmpty
    @Bind(R.id.name_edit)
    EditText mNameEditText;

    @NotEmpty
    @Email
    @Bind(R.id.email_edit)
    EditText mEmailEditText;

    @NotEmpty
    @Bind(R.id.public_repos_edit)
    EditText mPublicReposEditText;

    @NotEmpty
    @Bind(R.id.public_gists_edit)
    EditText mPublicGistsEditText;

    @NotEmpty
    @Bind(R.id.followers_edit)
    EditText mFollowersEditText;

    @NotEmpty
    @Bind(R.id.following_edit)
    EditText mFollowingEditText;

    @OnClick(R.id.done_button)
    void done() {
        if (mIsContentLoaded) {
            mValidator.validate();
        } else {
            Toast.makeText(getApplicationContext(), "Cannot validate unloaded content", Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.cancel_button)
    void cancel() {
        finish();
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, "Yay! we got it right!", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        ButterKnife.bind(this);

        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait. Loading...");
        mProgressDialog.setCancelable(true);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        // Need this to avoid crashes when backing out of web activity too fast
        if (!isFinishing()) {
            mProgressDialog.show();
        }

        Picasso.with(getApplicationContext())
                .load(R.drawable.placeholder)
                .into(image);

        Intent i = getIntent();
        String name = i.getStringExtra(getString(R.string.extra_name));

        GitClient.getGitClient().getUser(name, new Callback<User>() {
            @Override
            public void failure(RetrofitError error) {
                mIsContentLoaded = false;
                mProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failed to load", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(User user, Response response) {
                mIsContentLoaded = true;

                Picasso.with(getApplicationContext())
                        .load(user.getAvatar())
                        .into(image);

                mUsernameEditText.setText(user.getLogin());
                mNameEditText.setText(user.getName());
                mEmailEditText.setText(user.getEmail());
                mPublicReposEditText.setText(user.getPublicRepos());
                mPublicGistsEditText.setText(user.getPublicGists());
                mFollowersEditText.setText(String.valueOf(user.getFollowers()));
                mFollowingEditText.setText(String.valueOf(user.getFollowing()));

                mProgressDialog.dismiss();
            }
        });
    }
}