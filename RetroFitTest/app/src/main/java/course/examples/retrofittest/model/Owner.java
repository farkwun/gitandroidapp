package course.examples.retrofittest.model;

import com.google.gson.annotations.Expose;

/**
 * Created by Bryan Roiled on 2015-09-07.
 */
public class Owner {

    @Expose
    private String login;
    @Expose
    private String avatar_url;
    @Expose
    private String html_url;


    public String getLogin() {
        return this.login;
    }

    public String getAvatar_url() {
        return this.avatar_url;
    }

    public String getHtml_url() {
        return this.html_url;
    }


}
