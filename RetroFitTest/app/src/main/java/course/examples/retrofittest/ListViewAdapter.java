package course.examples.retrofittest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import course.examples.retrofittest.model.GitModel;


public class ListViewAdapter extends BaseAdapter implements Filterable {

    List<GitModel> mGitModelList;
    List<GitModel> mFilteredGitModelList;
    Context mContext;
    private final ItemFilter mFilter = new ItemFilter();

    private final OnImageViewClickedListener mOnImageViewClickedListener;
    public interface OnImageViewClickedListener {
        void onImageViewClicked(int position);
    }

    public ListViewAdapter(Context context, List<GitModel> gitModelList, OnImageViewClickedListener listener) {
        mGitModelList = gitModelList;
        mFilteredGitModelList = gitModelList;
        mContext = context;
        mOnImageViewClickedListener = listener;
    }

    @Override
    public int getCount() {
        return mFilteredGitModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilteredGitModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.activity_list_view, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Picasso.with(mContext)
                .load(mFilteredGitModelList.get(position).getOwner().getAvatar_url())
                .placeholder(R.drawable.placeholder)
                .into(holder.image);


        String name = mFilteredGitModelList.get(position).getName();
        int id = mFilteredGitModelList.get(position).getId();
        holder.text.setText(String.format(mContext.getString(R.string.listadapter_placeholder), name, id));
        holder.texttwo.setText(mFilteredGitModelList.get(position).getOwner().getLogin());

        if (mOnImageViewClickedListener != null) {
            holder.image.setOnClickListener(view -> mOnImageViewClickedListener.onImageViewClicked(position));
        }

        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.image_in_item)
        ImageView image;
        @Bind(R.id.textview_in_item)
        TextView text;
        @Bind(R.id.textview_in_item_two)
        TextView texttwo;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public void setGitmodel(List<GitModel> gitModelList) {
        clearGitmodel();
        this.mGitModelList = gitModelList;
        this.mFilteredGitModelList = gitModelList;
        notifyDataSetChanged();
    }

    public void clearGitmodel() {
        this.mGitModelList.clear();
        this.mFilteredGitModelList.clear();
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            final List<GitModel> list = mGitModelList;
            int count = list.size();
            final ArrayList<GitModel> nlist = new ArrayList<>(count);

            GitModel filterableGitModel;

            for (int i = 0; i < count; i++) {
                filterableGitModel = list.get(i);
                if (filterableGitModel.getOwner().getLogin().toLowerCase().contains(filterString)) {
                    nlist.add(filterableGitModel);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
            mFilteredGitModelList = (ArrayList<GitModel>) results.values;
            notifyDataSetChanged();
        }
    }
}