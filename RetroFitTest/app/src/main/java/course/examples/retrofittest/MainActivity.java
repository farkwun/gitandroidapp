package course.examples.retrofittest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import course.examples.retrofittest.API.GitClient;
import course.examples.retrofittest.model.GitModel;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity implements SwipeRefreshLayout.OnRefreshListener,
        ListViewAdapter.OnImageViewClickedListener {

    List<GitModel> mGitModel;
    ListViewAdapter mListAdapter;

    @Bind(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.empty)
    TextView mEmptyTextView;

    @Bind(R.id.listView)
    ListView mListView;

    @Bind(R.id.filterbar)
    EditText mFilterString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mFilterString.addTextChangedListener(searchTextWatcher);

        mGitModel = new ArrayList<>();
        mListAdapter = new ListViewAdapter(this, mGitModel, this);


        mListView.setEmptyView(mEmptyTextView);
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener((parent, view, position, id) -> {
            String url = mGitModel.get(position).getOwner().getHtml_url();
            // Launching new Activity on selecting image
            Intent i = new Intent(this, GitRepoWebActivity.class);
            // sending data to new activity
            i.putExtra(getString(R.string.extra_url), url);
            startActivity(i);
        });

        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    @Override
    public void onRefresh() {
        GitClient.getGitClient().getFeed(new Callback<List<GitModel>>() {
            @Override
            public void failure(RetrofitError error) {
                mSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getApplicationContext(), "Failed to load", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void success(final List<GitModel> gitmodel, Response response) {
                mGitModel = gitmodel;
                mSwipeRefreshLayout.setRefreshing(false);
                mListAdapter.setGitmodel(gitmodel);
            }
        });
    }

    private TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            mListAdapter.getFilter().filter(s.toString());
        }
    };

    @Override
    public void onImageViewClicked(int position) {
        String name = mGitModel.get(position).getOwner().getLogin();
        // Launching new Activity on selecting text
        Intent i = new Intent(this, UserProfileActivity.class);
        // sending data to new activity
        i.putExtra(getString(R.string.extra_name), name);
        startActivity(i);
    }
}

